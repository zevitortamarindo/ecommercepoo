create database BDEcommerce
go

use BDEcommerce
go

create table Produtos
(
	ProdutoId	int				primary key		identity,
	Nome		varchar(200)	not null,
	Preco		decimal(9,2)	not null,
)

INSERT INTO Produtos values ('L�pis', 1.50)
INSERT INTO Produtos values ('Caneta', 2.50)
INSERT INTO Produtos values ('Caderno', 7.20)

select * from Produtos

DELETE from Produtos where ProdutoId=4;

create table Clientes 
(	ClienteId		int				primary key		identity,
	Nome			varchar(50)		not null,
	Email			varchar(50)		not null,
	Senha			varchar(50)		not null,
	
	


);

insert into Clientes (Nome,Email,Senha) values
(	'Geraldo', 'geraldao@gmail.com','geraldasso'),
	('Roberta', 'robertinha@gmail.com','robertilds'),
	('Yuri Alberto', 'yuri9alberto@gmail.com','viciadoemgols');



select * from Clientes