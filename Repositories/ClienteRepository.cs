using System.Data.SqlClient;
public class ClienteRepository : Database, IClienteRepository
{
    public void Create(Cliente cliente)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Clientes VALUES (@nome, @email, @senha)";

        cmd.Parameters.AddWithValue("@nome",cliente.Name);
        cmd.Parameters.AddWithValue("@email",cliente.Email);
        cmd.Parameters.AddWithValue("@senha",cliente.Password);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Clientes WHERE ClienteId = @id";

        cmd.Parameters.AddWithValue("@id",id);


        cmd.ExecuteNonQuery();
    }

    public List<Cliente> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Clientes";

        SqlDataReader reader = cmd.ExecuteReader();
        List<Cliente> clientes = new List<Cliente>();
        while(reader.Read())
        {
            Cliente c = new Cliente();
            c.ClienteId = reader.GetInt32(0);
            c.Name= reader.GetString(1);
            c.Email= reader.GetString(2);
            c.Password= reader.GetString(3);
            clientes.Add(c);
        }

        return clientes;
    }

    public Cliente Read(int id)
    {
        
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM Clientes WHERE ClienteId = @id";

            cmd.Parameters.AddWithValue("@id", id);

            SqlDataReader reader = cmd.ExecuteReader();

            if(reader.Read()) 
            {
                Cliente c = new Cliente();
                c.ClienteId = reader.GetInt32(0);
                c.Name = reader.GetString(1);
                c.Email = reader.GetString(2);
                c.Password = reader.GetString(3);

                return c;
            }

        return null;
            

        
    }

    public void Update(int id, Cliente cliente)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = @"UPDATE Clientes 
                            SET Nome = @name, Email = @email, Senha = @password 
                            WHERE ClienteId = @id";

        cmd.Parameters.AddWithValue("@nome", cliente.Name);
        cmd.Parameters.AddWithValue("@email", cliente.Email);
        cmd.Parameters.AddWithValue("@password", cliente.Password);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }
    public List<Cliente> Search(String pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Clientes WHERE Nome LIKE @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");

        SqlDataReader reader = cmd.ExecuteReader();
        List<Cliente> clientes = new List<Cliente>();
        while(reader.Read())
        {
            Cliente c = new Cliente();
            c.ClienteId = reader.GetInt32(0);
            c.Name= reader.GetString(1);
            c.Email= reader.GetString(2);
            c.Password= reader.GetString(3);
            clientes.Add(c);
        }

        return clientes;

    }

}