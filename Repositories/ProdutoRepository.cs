using System.Data.SqlClient;
public class ProdutoRepository : Database, IProdutoRepository
{
    public void Create(Produto produto)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "INSERT INTO Produtos VALUES (@nome, @preco)";

        cmd.Parameters.AddWithValue("@nome",produto.Name);
        cmd.Parameters.AddWithValue("@preco",produto.Price);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int id)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Produtos WHERE ProdutoId = @id";

        cmd.Parameters.AddWithValue("@id",id);


        cmd.ExecuteNonQuery();
    }

    public List<Produto> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos";

        SqlDataReader reader = cmd.ExecuteReader();
        List<Produto> produtos = new List<Produto>();
        while(reader.Read())
        {
            Produto p = new Produto();
            p.ProdutoId = reader.GetInt32(0);
            p.Name= reader.GetString(1);
            p.Price= reader.GetDecimal(2);
            produtos.Add(p);
        }

        return produtos;
    }

    public Produto Read(int id)
    {
        
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT * FROM Produtos WHERE ProdutoId = @id";

            cmd.Parameters.AddWithValue("@id", id);

            SqlDataReader reader = cmd.ExecuteReader();

            if(reader.Read()) 
            {
                Produto p = new Produto();
                p.ProdutoId = reader.GetInt32(0);
                p.Name = reader.GetString(1);
                p.Price = reader.GetDecimal(2);

                return p;
            }

        return null;
            

        
    }

    public void Update(int id, Produto produto)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = @"UPDATE Produtos 
                            SET Nome = @name, Preco = @price 
                            WHERE ProdutoId = @id";

        cmd.Parameters.AddWithValue("@name", produto.Name);
        cmd.Parameters.AddWithValue("@price", produto.Price);
        cmd.Parameters.AddWithValue("@id", id);

        cmd.ExecuteNonQuery();

    }
}