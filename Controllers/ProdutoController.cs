using Microsoft.AspNetCore.Mvc;

public class ProdutoController : Controller
{
    // http://localhost:5024/produto/index
    
    IProdutoRepository produtoRepository;

    public ProdutoController(IProdutoRepository produtoRepository)
    {
        this.produtoRepository = produtoRepository;
    }

     
    public ActionResult Index()
    {   
        return View(produtoRepository.Read());
    }
    // Retornar dados = API retornar View = MVC

    // http://localhost:5024/produto/create
    [HttpGet]
    public ActionResult Create()
    {
        // /Views/Produto/Create.cshtml
        return View();

    }
    [HttpPost]
    public ActionResult Create(Produto model)
    {
        produtoRepository.Create(model);
        return RedirectToAction("Index");
    }

    /*
    [HttpPost]
    public ActionResult Create(IFormCollection form)
    {
        string nome = form["nome"];
        string preco = form["preco"];
        
        Produto p = new Produto();
        p.Name = nome;
        p.Price = decimal.Parse(preco); 
        lista.Add(p);
        return RedirectToAction("Index");
    }
    */
    public ActionResult Details(int id)
    {   
        Produto p = produtoRepository.Read(id);
        if(p != null)
            return View(p);
        return NotFound();
    }
    public ActionResult Delete(int id)
    {
        produtoRepository.Delete(id);
        
        return RedirectToAction("Index");
    }
    public ActionResult Update(int id)
    {
        
        Produto p = produtoRepository.Read(id);
        if(p != null)
            return View(p);
        
        return NotFound();

    }
    [HttpPost]
    public ActionResult Update(int id, Produto model)
    {
        produtoRepository.Update(id,model);
        return RedirectToAction("Index");
    }


}
   
    


