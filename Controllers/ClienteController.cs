using Microsoft.AspNetCore.Mvc;

public class ClienteController : Controller

{
    IClienteRepository clienteRepository;
    static List<Cliente> listaC = new List<Cliente>();
    public ClienteController(IClienteRepository clienteRepository)
    {
        this.clienteRepository = clienteRepository;
    }

    static List<Cliente> lista1 = new List<Cliente>();
    public ActionResult Index()
    {
        return View(clienteRepository.Read());
    }
    [HttpGet]
    public ActionResult Create()
    {
        // /Views/Cliente/Create.cshtml
        return View();

    }
    [HttpPost]
    public ActionResult Create(Cliente model)
    {
        clienteRepository.Create(model);
        return RedirectToAction("Index");
    }
    public ActionResult Details(int id)
    {   
        Cliente c = clienteRepository.Read(id);
        if(c != null)
            return View(c);
        return NotFound();
        
    }
    public ActionResult Delete(int id)
    {
        clienteRepository.Delete(id);
        return RedirectToAction("Index");
    }
    public ActionResult Update(int id)
    {
        Cliente c = clienteRepository.Read(id);
            if(c.ClienteId == id)
                return View(c);    
        
        return NotFound();
    }
    [HttpPost]
    public ActionResult Update(int id, Cliente model)
    {
        clienteRepository.Update(id,model);
        return RedirectToAction("Index");
    }
     public ActionResult Search(IFormCollection form)
    {
        string pesquisa = form["pesquisa"].ToString();
        return View("Index", clienteRepository.Search(pesquisa));
    }


}
    




