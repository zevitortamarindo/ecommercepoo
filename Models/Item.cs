public class Item
{
    public Produto Product { get; set; }
    public int Ammount { get; set; }
    public decimal Total 
    {
        get {return this.Product.Price*this.Ammount;}
    }
}