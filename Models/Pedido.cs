public class Pedido
{
    public int PedidoId { get; set; }
    public DateTime Date { get; set; }
    public decimal Total => Items.Sum(item => item.Total);
    
    /* 
    {
        
        get
        {
            decimal total=0;
            foreach(var item in Items)
            {
                total += item.Total;
            }
            return total;
        }
     }
    */
    
    public Cliente Client { get; set; }
    public List<Item> Items { get; set; }
}