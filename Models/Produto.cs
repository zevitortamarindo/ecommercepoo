public class Produto
{
    public int ProdutoId { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
}